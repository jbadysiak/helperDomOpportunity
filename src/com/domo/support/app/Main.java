package com.domo.support.app;

import com.domo.client.exceptions.RequestFailedException;
import com.domo.support.app.alertboxes.AlertBox;
import com.domo.support.app.alertboxes.ConfirmBox;
import com.domo.support.app.controller.Controller;
import com.domo.support.app.domo.Data;
import com.domo.support.app.domo.DomoConnector;
import com.domo.support.app.exception.LoggingNullPointerException;
import com.domo.support.app.file.FileSupport;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.List;

public class Main extends Application {

    private Stage window;
    private Scene startingScene, resultsScene;
    private ObservableList<Data> listOfData;
    private String supportChoice;
    private TableView<Data> table;
    private String pathToCbiDataflowsIds = "C:\\CBI\\DomoApp\\CbiDataflowIds.txt";
    private String pathToCbiDatasetsIds = "C:\\CBI\\DomoApp\\CbiDatasetsIds.txt";
    private String pathToTransFixDataflowsIds = "C:\\CBI\\DomoApp\\TDataflowIds.txt";
    private String pathToTransFixDatasetsIds = "C:\\CBI\\DomoApp\\TDatasetsIds.txt";
    private List<String> listOfDataflowsData = null;
    private List<String> listOfDatasetsData = null;
    private String domain;
    private String domoApiToken;

    @Override
    public void start(Stage primaryStage) {
        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        StringBuilder domainStringBulider = new StringBuilder();
        DomoConnector domoConnector = new DomoConnector();

        gridPane.setHgap(5);
        gridPane.setVgap(5);
        Controller controller = new Controller();
        window = primaryStage;
        window.setTitle("Support DOMO PG App");
        window.setResizable(false);
        window.setOnCloseRequest(event -> {
            event.consume();
            closeProgram();
        });

        Label labelApiToken = new Label("Api Token");
        Label labelDomain = new Label("Domain");
        Label labelProject = new Label("Project");
        final TextField tfApiToken = new TextField();
        final TextField tfDomain = new TextField();

        ChoiceBox choiceBoxSupportDataflows = controller.getChoiceBox();
        choiceBoxSupportDataflows.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.UP || event.getCode() == KeyCode.DOWN) {
                    event.consume();
                }
            }
        });
        choiceBoxSupportDataflows.getItems().addAll("CBI", "T&W BI");

        TableColumn<Data, String> nameColumn = controller.getNameColumn();
        TableColumn<Data, String> idColumn = controller.getIdColumn();
        TableColumn<Data, String> typeColumn = controller.getTypeDataColumn();
        TableColumn<Data, String> statusColumn = controller.getStatusColumn();
        TableColumn<Data, String> beginDateColumn = controller.getBeginDateColumn();
        TableColumn<Data, String> endDateColumn = controller.getEndDateColumn();
        TableColumn<Data, String> expectedEndDateColumn = controller.getExpectedEndDateColumn();
        TableColumn<Data, String> onTimeColumn = controller.getOnTimeColumn();
        TableColumn<Data, String> lastDurationColumn = controller.getLastDurationColumn();
        TableColumn<Data, String> avgDurationColumn = controller.getAvgDurationColumn();
        TableColumn<Data, String> versionColumn = controller.getVersionColumn();
        TableColumn<Data, String> rowsReadColumn = controller.getRowsReadColumn();
        TableColumn<Data, String> avgRowsReadColumn = controller.getAvgRowsReadColumn();
        TableColumn<Data, String> bytesReadColumn = controller.getBytesReadColumn();
        TableColumn<Data, String> avgBytesReadColumn = controller.getAvgBytesReadColumn();
        TableColumn<Data, String> rowsWriteColumn = controller.getRowsWriteColumn();
        TableColumn<Data, String> rowsWriteLastTimeColumn = controller.getRowsWriteLastTimeColumn();
        TableColumn<Data, String> avgRowsWriteColumn = controller.getAvgRowsWriteColumn();
        TableColumn<Data, String> bytesWriteColumn = controller.getBytesWriteColumn();
        TableColumn<Data, String> avgBytesWriteColumn = controller.getAvgBytesWriteColumn();

        table = new TableView<>();
        table.getColumns().addAll(nameColumn, idColumn, typeColumn, statusColumn, beginDateColumn, endDateColumn,
                expectedEndDateColumn, onTimeColumn, lastDurationColumn, avgDurationColumn, versionColumn,
                rowsReadColumn, avgRowsReadColumn, bytesReadColumn, avgBytesReadColumn, rowsWriteColumn,
                rowsWriteLastTimeColumn, avgRowsWriteColumn, bytesWriteColumn, avgBytesWriteColumn);

        VBox resultsVBox = controller.getResultsVBox();

        Button refreshButton = new Button("Refresh!");
        refreshButton.setMinWidth(180);
        refreshButton.setPadding(new Insets(5));
        refreshButton.setOnAction(e -> {
            listOfData = domoConnector.createListOfData(listOfDataflowsData, listOfDatasetsData);
            table.setItems(listOfData);
        });

        Button downloadButton = new Button("Check data!");
        downloadButton.setMinWidth(180);
        downloadButton.setOnAction(e -> {
            try {
                supportChoice = (String) choiceBoxSupportDataflows.getValue();
                domoApiToken = tfApiToken.getText().trim();
                domain = domainStringBulider.append(tfDomain.getText().trim().toLowerCase()).append(".domo.com").toString();
                domainStringBulider.setLength(0);
                checkIfValuesEqualsNull(domoApiToken, domain, supportChoice);

                domoConnector.ensureAuthDataManager(domoApiToken, domain);
            } catch (RequestFailedException e1) {
                AlertBox.display(String.valueOf(e1.getFailureType()), "Your Api Token is unauthorized!\nTry log in again!");
                return;
            } catch (LoggingNullPointerException e1) {
                AlertBox.display("Null Error", e1.getMessage());
                return;
            }

            switch (supportChoice) {
                case "CBI":
                    listOfDataflowsData = new FileSupport().getDataIdsList(pathToCbiDataflowsIds, "CBI");
                    listOfDatasetsData = new FileSupport().getDataIdsList(pathToCbiDatasetsIds, "CBI");
                    break;
                case "T&W BI":
                    listOfDataflowsData = new FileSupport().getDataIdsList(pathToTransFixDataflowsIds, "T&W BI");
                    listOfDatasetsData = new FileSupport().getDataIdsList(pathToTransFixDatasetsIds, "T&W BI");
                    break;
            }

            try {
                if (listOfDataflowsData.equals(null) || listOfDatasetsData.equals(null)) {
                    throw new NullPointerException();
                }
            } catch (NullPointerException e1){
                return;
            }

            listOfData = domoConnector.createListOfData(listOfDataflowsData, listOfDatasetsData);
            table.setItems(listOfData);
            table.setSelectionModel(null);

            onTimeColumn.setCellFactory(column -> {
                return new TableCell<Data, String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);

                        setText(empty ? "" : getItem());
                        setGraphic(null);

                        TableRow<Data> currentRow = getTableRow();

                        currentRow.getTableView().getItems();

                        if (!empty) {
                            if (item.equals("true")) {
                                currentRow.setStyle("-fx-background-color:lightgreen");
                            } else {
                                currentRow.setStyle("-fx-background-color:orange");
                            }
                        }
                    }
                };
            });

            resultsVBox.getChildren().addAll(table, refreshButton);

            resultsScene = new Scene(resultsVBox, 1500, 450);
            window.setResizable(true);
            window.setScene(resultsScene);
        });

        gridPane.add(labelApiToken, 0, 0);
        gridPane.add(tfApiToken, 1, 0);
        gridPane.add(labelDomain, 0, 1);
        gridPane.add(tfDomain, 1, 1);
        gridPane.add(labelProject, 0, 2);
        gridPane.add(choiceBoxSupportDataflows, 1, 2);
        gridPane.add(downloadButton, 1, 3);

        startingScene = new Scene(gridPane, 300, 200);
        window.setScene(startingScene);
        window.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private void closeProgram() {
        Boolean answer = ConfirmBox.display("Exit", "Are you sure want to exit?");
        if (answer)
            window.close();
    }

    private void checkIfValuesEqualsNull(String domoApiToken, String domain1, String supportChoice) {
        if (domoApiToken.isEmpty() && domain1.isEmpty() && supportChoice == null) {
            throw new LoggingNullPointerException("You typed nothing in the form");
        }
        if (domoApiToken.isEmpty() && domain1.isEmpty()) {
            throw new LoggingNullPointerException("You 'Api Token' and 'Domain' are equals null");
        }
        if (supportChoice == null && domain1.isEmpty()) {
            throw new LoggingNullPointerException("You 'Choice' and 'domain' are equals null");
        }
        if (supportChoice == null && domoApiToken.isEmpty()) {
            throw new LoggingNullPointerException("You 'Choice' and 'Api Token' are equals null");
        }
        if (domoApiToken.isEmpty()) {
            throw new LoggingNullPointerException("Your 'Api Token' is equal null");
        }
        if (domain1.isEmpty()) {
            throw new LoggingNullPointerException("Your 'Domain' is equal null");
        }
        if (supportChoice == null) {
            throw new LoggingNullPointerException("Your 'Choice' is equal null");
        }
    }

}

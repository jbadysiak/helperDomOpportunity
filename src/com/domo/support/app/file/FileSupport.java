package com.domo.support.app.file;

import com.domo.support.app.alertboxes.AlertBox;
import com.domo.support.app.domo.Data;
import javafx.collections.ObservableList;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileSupport {
    public List<String> getDataIdsList(String pathToFile, String project) {
        List<String> dataIds;
        try (Stream<String> stream = Files.lines(Paths.get(pathToFile))) {
            dataIds = stream.collect(Collectors.toList());
        } catch (IOException e) {
            List<String> listOfSplittedPath = Arrays.asList(pathToFile.split(Pattern.quote(System.getProperty("file.separator"))));
            AlertBox.display("Incorrect path to file", "Check if you set the correct path to " + listOfSplittedPath.get(listOfSplittedPath.size() - 1) + " file to "+ project +" project.\n" +
                    "The obligatory path you can find in a readme.txt file.");
            return null;
        }
        return dataIds;
    }

    public void writeToExcelFile(ObservableList<Data> listOfData) {
        Workbook workbook = new HSSFWorkbook();

        Sheet sheetDataOfExecutions = workbook.createSheet("Data of executions");

        Font headerFont = workbook.createFont();
        headerFont.setBoldweight((short) 5);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.AQUA.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheetDataOfExecutions.createRow(0);

        for (int i = 0; i < nameOfColumns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(nameOfColumns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        listOfData.stream().forEach(data -> {
            int iterator = sheetDataOfExecutions.getLastRowNum() + 1;
            Row row = sheetDataOfExecutions.createRow(iterator);

            if (data.getStatus() == null && data.getType() == null) {
                row.createCell(0).setCellValue(data.getName());
                row.createCell(1).setCellValue(data.getId());
                row.createCell(7).setCellValue(data.getOnTime());
            }

            if (data.getVersion() == null && data.getStatus() != null) {
                row.createCell(0).setCellValue(data.getName());
                row.createCell(1).setCellValue(data.getId());
                row.createCell(2).setCellValue(data.getStatus());
                row.createCell(3).setCellValue(data.getType());
                row.createCell(5).setCellValue(data.getEndDate());
                row.createCell(6).setCellValue(data.getExpectedEndDate());
                row.createCell(7).setCellValue(data.getOnTime());
                row.createCell(15).setCellValue(data.getRowsWrite());
            } else if (data.getStatus() != null) {
                if (data.getStatus().equals("RUNNING_DATA_FLOW")) {
                    row.createCell(0).setCellValue(data.getName());
                    row.createCell(1).setCellValue(data.getId());
                    row.createCell(2).setCellValue(data.getStatus());
                    row.createCell(3).setCellValue(data.getType());
                    row.createCell(4).setCellValue(data.getBeginDate());
                    row.createCell(5).setCellValue(data.getEndDate());
                    row.createCell(6).setCellValue(data.getExpectedEndDate());
                    row.createCell(7).setCellValue(data.getOnTime());
                    row.createCell(8).setCellValue(data.getLastDuration());
                    row.createCell(9).setCellValue(data.getAvgDuration());
                    row.createCell(10).setCellValue(data.getVersion());
                    row.createCell(12).setCellValue(data.getAvgRowsRead());
                    row.createCell(14).setCellValue(data.getAvgBytesRead());
                    row.createCell(16).setCellValue(data.getAvgRowsWrite());
                    row.createCell(18).setCellValue(data.getAvgBytesWrite());
                } else {
                    row.createCell(0).setCellValue(data.getName());
                    row.createCell(1).setCellValue(data.getId());
                    row.createCell(2).setCellValue(data.getStatus());
                    row.createCell(3).setCellValue(data.getType());
                    row.createCell(4).setCellValue(data.getBeginDate());
                    row.createCell(5).setCellValue(data.getEndDate());
                    row.createCell(6).setCellValue(data.getExpectedEndDate());
                    row.createCell(7).setCellValue(data.getOnTime());
                    row.createCell(8).setCellValue(data.getLastDuration());
                    row.createCell(9).setCellValue(data.getAvgDuration());
                    row.createCell(10).setCellValue(data.getVersion());
                    row.createCell(11).setCellValue(data.getRowsRead());
                    row.createCell(12).setCellValue(data.getAvgRowsRead());
                    row.createCell(13).setCellValue(data.getBytesRead());
                    row.createCell(14).setCellValue(data.getAvgBytesRead());
                    row.createCell(15).setCellValue(data.getRowsWrite());
                    row.createCell(16).setCellValue(data.getAvgRowsWrite());
                    row.createCell(17).setCellValue(data.getBytesWrite());
                    row.createCell(18).setCellValue(data.getAvgBytesWrite());
                }
            }
        });

        for (int i = 0; i < nameOfColumns.length; i++) {
            sheetDataOfExecutions.autoSizeColumn(i);
        }

        try {
            FileOutputStream fileOutputStream = new FileOutputStream("C:\\CBI\\DomoApp\\extractsData" + getCurrentDate() + ".xls");
            workbook.write(fileOutputStream);
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getCurrentDate() {
        Date curDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String dateToString = sdf.format(curDate);
        return dateToString;
    }

    private static String[] nameOfColumns = {"Name", "Id", "Type", "Status", "Begin date", "End date", "End expected end",
            "On time", "Last duration", "Avg duration", "Version", "Rows read", "Avg rows read", "Bytes read",
            "Avg bytes read", "Rows write", "Avg rows write", "Bytes write", "Avg bytes write"};

}

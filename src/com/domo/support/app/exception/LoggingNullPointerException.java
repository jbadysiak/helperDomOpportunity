package com.domo.support.app.exception;

public class LoggingNullPointerException extends NullPointerException {
    public LoggingNullPointerException() {
    }

    public LoggingNullPointerException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}

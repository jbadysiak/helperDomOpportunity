package com.domo.support.app.controller;

import com.domo.support.app.domo.Data;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class Controller {

    public TableColumn<Data, String> getNameColumn() {
        TableColumn<Data, String> nameColumn = new TableColumn<>("Name");
        nameColumn.setMinWidth(200);
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        return nameColumn;
    }

    public TableColumn<Data, String> getIdColumn() {
        TableColumn<Data, String> dataflowIdColumn = new TableColumn<>("Id");
        dataflowIdColumn.setMinWidth(50);
        dataflowIdColumn.setPrefWidth(50);
        dataflowIdColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        return dataflowIdColumn;
    }

    public TableColumn<Data, String> getTypeDataColumn() {
        TableColumn<Data, String> dataflowIdColumn = new TableColumn<>("Type");
        dataflowIdColumn.setMinWidth(40);
        dataflowIdColumn.setCellValueFactory(new PropertyValueFactory<>("type"));
        return dataflowIdColumn;
    }

    public TableColumn<Data, String> getStatusColumn() {
        TableColumn<Data, String> statusColumn = new TableColumn<>("Status");
        statusColumn.setMinWidth(50);
        statusColumn.setCellValueFactory(new PropertyValueFactory<>("status"));
        return statusColumn;
    }

    public TableColumn<Data, String> getBeginDateColumn() {
        TableColumn<Data, String> dateColumn = new TableColumn<>("Begin Date");
        dateColumn.setMinWidth(80);
        dateColumn.setCellValueFactory(new PropertyValueFactory<>("beginDate"));
        return dateColumn;
    }

    public TableColumn<Data, String> getEndDateColumn() {
        TableColumn<Data, String> dateColumn = new TableColumn<>("End Date");
        dateColumn.setMinWidth(80);
        dateColumn.setCellValueFactory(new PropertyValueFactory<>("endDate"));
        return dateColumn;
    }

    public TableColumn<Data, String> getExpectedEndDateColumn() {
        TableColumn<Data, String> expectedEndDateColumn = new TableColumn<>("Expected End Date");
        expectedEndDateColumn.setMinWidth(100);
        expectedEndDateColumn.setCellValueFactory(new PropertyValueFactory<>("expectedEndDate"));
        return expectedEndDateColumn;
    }

    public TableColumn<Data, String> getOnTimeColumn() {
        TableColumn<Data, String> onTimeColumn = new TableColumn<>("End in Time");
        onTimeColumn.setMinWidth(100);
        onTimeColumn.setCellValueFactory(new PropertyValueFactory<>("onTime"));
        return onTimeColumn;
    }

    public TableColumn<Data, String> getLastDurationColumn() {
        TableColumn<Data, String> durationColumn = new TableColumn<>("Last Duration");
        durationColumn.setMinWidth(75);
        durationColumn.setCellValueFactory(new PropertyValueFactory<>("lastDuration"));
        return durationColumn;
    }

    public TableColumn<Data, String> getAvgDurationColumn() {
        TableColumn<Data, String> durationColumn = new TableColumn<>("Avg Duration");
        durationColumn.setMinWidth(75);
        durationColumn.setCellValueFactory(new PropertyValueFactory<>("avgDuration"));
        return durationColumn;
    }

    public TableColumn<Data, String> getVersionColumn() {
        TableColumn<Data, String> versionColumn = new TableColumn<>("Version");
        versionColumn.setMinWidth(45);
        versionColumn.setCellValueFactory(new PropertyValueFactory<>("version"));
        return versionColumn;
    }

    public TableColumn<Data, String> getRowsReadColumn() {
        TableColumn<Data, String> rowsReadColumn = new TableColumn<>("Rows Read");
        rowsReadColumn.setMinWidth(80);
        rowsReadColumn.setCellValueFactory(new PropertyValueFactory<>("rowsRead"));
        return rowsReadColumn;
    }

    public TableColumn<Data, String> getAvgRowsReadColumn() {
        TableColumn<Data, String> avgRowsReadColumn = new TableColumn<>("Avg Rows Read");
        avgRowsReadColumn.setMinWidth(100);
        avgRowsReadColumn.setCellValueFactory(new PropertyValueFactory<>("avgRowsRead"));
        return avgRowsReadColumn;
    }

    public TableColumn<Data, String> getBytesReadColumn() {
        TableColumn<Data, String> bytesReadColumn = new TableColumn<>("Bytes Read");
        bytesReadColumn.setMinWidth(80);
        bytesReadColumn.setCellValueFactory(new PropertyValueFactory<>("bytesRead"));
        return bytesReadColumn;
    }

    public TableColumn<Data, String> getAvgBytesReadColumn() {
        TableColumn<Data, String> avgBytesReadColumn = new TableColumn<>("Avg Bytes Read");
        avgBytesReadColumn.setMinWidth(100);
        avgBytesReadColumn.setCellValueFactory(new PropertyValueFactory<>("avgBytesRead"));
        return avgBytesReadColumn;
    }

    public TableColumn<Data, String> getRowsWriteColumn() {
        TableColumn<Data, String> rowsWriteColumn = new TableColumn<>("Rows Write");
        rowsWriteColumn.setMinWidth(80);
        rowsWriteColumn.setCellValueFactory(new PropertyValueFactory<>("rowsWrite"));
        return rowsWriteColumn;
    }

    public TableColumn<Data, String> getRowsWriteLastTimeColumn() {
        TableColumn<Data, String> rowsWriteLastTimeColumn = new TableColumn<>("Rows Write Last Time");
        rowsWriteLastTimeColumn.setMinWidth(80);
        rowsWriteLastTimeColumn.setCellValueFactory(new PropertyValueFactory<>("rowsWriteLastTime"));
        return rowsWriteLastTimeColumn;
    }

    public TableColumn<Data, String> getAvgRowsWriteColumn() {
        TableColumn<Data, String> avgRowsWriteColumn = new TableColumn<>("Avg Rows Write");
        avgRowsWriteColumn.setMinWidth(100);
        avgRowsWriteColumn.setCellValueFactory(new PropertyValueFactory<>("avgRowsWrite"));
        return avgRowsWriteColumn;
    }

    public TableColumn<Data, String> getBytesWriteColumn() {
        TableColumn<Data, String> bytesWriteColumn = new TableColumn<>("Bytes Write");
        bytesWriteColumn.setMinWidth(80);
        bytesWriteColumn.setCellValueFactory(new PropertyValueFactory<>("bytesWrite"));
        return bytesWriteColumn;
    }

    public TableColumn<Data, String> getAvgBytesWriteColumn() {
        TableColumn<Data, String> avgBytesWriteColumn = new TableColumn<>("Avg Bytes Write");
        avgBytesWriteColumn.setMinWidth(100);
        avgBytesWriteColumn.setCellValueFactory(new PropertyValueFactory<>("avgBytesWrite"));
        return avgBytesWriteColumn;
    }

    public VBox getResultsVBox() {
        VBox resultsVBox = new VBox();
        resultsVBox.setSpacing(5);
        resultsVBox.setAlignment(Pos.CENTER);
        resultsVBox.setPadding(new Insets(10,10,10,10));
        return resultsVBox;
    }

    public HBox getButtonsHBox() {
        HBox buttonsHBox = new HBox(10);
        buttonsHBox.setAlignment(Pos.BOTTOM_LEFT);
        return buttonsHBox;
    }

    public VBox getStartVBox() {
        VBox startVBox = new VBox(10);
        startVBox.setPadding(new Insets(10,10,10,10));
        startVBox.setAlignment(Pos.TOP_LEFT);
        return startVBox;
    }

    public ChoiceBox getChoiceBox(){
        ChoiceBox choiceBox = new ChoiceBox();
        choiceBox.setMinWidth(180);
        return choiceBox;
    }
}

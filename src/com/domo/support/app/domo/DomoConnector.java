package com.domo.support.app.domo;

import com.domo.client.DomoClient;
import com.domo.client.auth.DomoApiTokenAuth;
import com.domo.client.exceptions.RequestFailedException;
import com.domo.client.manager.DataFlowManager;
import com.domo.client.manager.DatasetManager;
import com.domo.client.model.dataflow.Dataflow;
import com.domo.client.model.dataflow.DataflowExecutions;
import com.domo.client.model.datasources.DataFusion;
import com.domo.client.model.datasources.Dataset;
import com.domo.client.model.datasources.DatasetVersion;
import com.domo.client.model.datasources.DomoDataStream;
import com.domo.support.app.alertboxes.AlertBox;
import com.domo.support.app.file.FileSupport;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class DomoConnector {

    private DomoApiTokenAuth domoApiTokenAuth;
    private static DatasetManager datasetManager;
    private static DataFlowManager dataFlowManager;
    private ObservableList<Data> listOfData;

    public DomoConnector() {
    }

    public void ensureAuthDataManager(String apiToken, String domain) throws RequestFailedException {
        domoApiTokenAuth = new DomoApiTokenAuth(apiToken);
        DomoClient domoClient = new DomoClient(domoApiTokenAuth, domain);
        dataFlowManager = new DataFlowManager(domoClient);
        datasetManager = new DatasetManager(domoClient);
        dataFlowManager.getDataflows();
    }

    public ObservableList<Data> createListOfData(List<String> listOfDataflowsData, List<String> listOfDatasetsData) {
        FileSupport writtenToExcelFile = new FileSupport();
        listOfData = FXCollections.observableArrayList();

        listOfDataflowsData.stream().forEach(dataflowData -> {
            Data data;
            List<String> listOfDataflowData = Arrays.asList((dataflowData.split(",")));
            String expectedTime = getExpectedDate(listOfDataflowData);
            Dataflow dataflow = getDataflowById(listOfDataflowData.get(0));
            List<DataflowExecutions> listOfDataflowExecutions = getDataflowExecutionsByDataflowId(listOfDataflowData.get(0));

            if (dataflow != null) {
                data = setDataFromDataflow(dataflow, listOfDataflowExecutions, expectedTime);
            } else {
                data = setDataNull(listOfDataflowData.get(0));
            }

            listOfData.add(data);
        });

        listOfDatasetsData.stream().forEach(datasetData -> {
            Data data;
            List<String> listOfDatasetData = Arrays.asList(datasetData.split(","));
            String expectedTime = getExpectedDate(listOfDatasetData);
            Dataset dataset = getDatasetById(listOfDatasetData.get(0));

            if (dataset != null) {
                data = setDataFromDataset(dataset, expectedTime);
            } else {
                data = setDataNull(listOfDatasetData.get(0));
            }

            listOfData.add(data);
        });

        if (listOfData != null)
            writtenToExcelFile.writeToExcelFile(listOfData);

        return listOfData;
    }

    public String switchLongToDate(long time) {
        Date lastUpdatedDate = new Date(time);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        return sdf.format(lastUpdatedDate);
    }

    public long switchStringToTimeInLong(String time) {
        long expectedDateLong = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        try {
            Date expectedDateHelp = sdf.parse(time);
            expectedDateLong = expectedDateHelp.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return expectedDateLong;
    }

    public String compareEndAndExpectedTime(long endTimeOfProcessing, String expectedDate) {
        long expectedDateLong = switchStringToTimeInLong(expectedDate);
        if (endTimeOfProcessing < expectedDateLong) {
            return String.valueOf(true);
        } else {
            return String.valueOf(false);
        }
    }

    public String compareEndAndExpectedTime(DataflowExecutions dataflowExecution, String expectedDate) {
        long expectedDateLong = switchStringToTimeInLong(expectedDate);
        long endTimeOfProcessing;
        String endTimeOfProcessingString;

        if (dataflowExecution.get(DataflowExecutions.MetadataKey.state).equals("RUNNING_DATA_FLOW")
                || dataflowExecution.get(DataflowExecutions.MetadataKey.state).equals("CREATED")
                || dataflowExecution.get(DataflowExecutions.MetadataKey.state).equals("FAILED_DATA_FLOW")) {
            return String.valueOf(false);
        } else {
            endTimeOfProcessingString = switchLongToDate(dataflowExecution.get(DataflowExecutions.MetadataKey.endTime));
            endTimeOfProcessing = switchStringToTimeInLong(endTimeOfProcessingString);
        }

        if (endTimeOfProcessing < expectedDateLong) {
            return String.valueOf(true);
        } else {
            return String.valueOf(false);
        }
    }

    private Dataflow getDataflowById(String dataflowId) {
        Dataflow dataflow;
        try {
            dataflow = dataFlowManager.getDataflow(Long.valueOf(dataflowId));
        } catch (RequestFailedException e) {
            e.printStackTrace();
            AlertBox.display(e.getFailureType() + " ACTION!", "Access denied to " + dataflowId + " dataflowId!");
            return null;
        }
        return dataflow;
    }

    private Dataset getDatasetById(String datasetId) {
        Dataset dataset;
        try {
            dataset = datasetManager.get(datasetId);
        } catch (RequestFailedException e) {
            e.printStackTrace();
            AlertBox.display(e.getFailureType() + " ACTION!", "Access denied to " + datasetId + " datasetId!");
            return null;
        }
        return dataset;
    }

    private List<DataflowExecutions> getDataflowExecutionsByDataflowId(String dataflowId) {
        List<DataflowExecutions> dataflowExecutions;
        try {
            dataflowExecutions = dataFlowManager.getDataflowExecutions(Long.valueOf(dataflowId));
        } catch (RequestFailedException e) {
            e.printStackTrace();
            return null;
        }
        return dataflowExecutions;
    }

    private Data setDataNull(String dataId) {
        Data data = new Data();
        data.setId(dataId);
        data.setName("Access is denied");
        data.setOnTime("false");
        return data;
    }

    private Data setDataFromDataset(Dataset dataset, String expectedTime) {
        Data data = new Data();
        data.setName(dataset.getName());
        data.setId(dataset.getId());
        data.setType("dataset");
        data.setStatus(dataset.getStatus());
        data.setEndDate(switchLongToDate(dataset.getLastTouchedUnixTimestampExtendedUtc()));
        data.setRowsWrite(dataset.getRowCount());
        data.setExpectedEndDate(expectedTime);
        data.setOnTime(compareEndAndExpectedTime(dataset.getLastTouchedUnixTimestampExtendedUtc(), expectedTime));

        if (dataset.getStreamId() != null) {
            DomoDataStream domoDataStream = null;
            DataFusion dataFusion = null;
            DatasetVersion datasetVersion = null;

            try {
                domoDataStream = datasetManager.getDataStreamDefinition(dataset.getStreamId());
                dataFusion = datasetManager.getDataFusion(dataset.getId());
                datasetVersion = datasetManager.getLatestDatasourceVersionDetails(dataset.getId());

            } catch (RequestFailedException e) {
                e.printStackTrace();
            }
        }
        return data;
    }

    private String setEndTimeOfExecutionDataflow(DataflowExecutions dataflowExecution) {
        String endTimeOfProcessing;
        try {
            endTimeOfProcessing = switchLongToDate(dataflowExecution.get(DataflowExecutions.MetadataKey.endTime));
        } catch (NullPointerException e) {
            return "---";
        }
        return endTimeOfProcessing;
    }

    private Data setDataFromDataflow(Dataflow dataflow, List<DataflowExecutions> listOfDataflowExecutions, String expectedTime) {
        Data data = new Data();
        data.setName(dataflow.get(Dataflow.MetadataKey.name));
        data.setId(Integer.toString(dataflow.get(Dataflow.MetadataKey.id)));
        data.setType("dataflow");
        data.setStatus(listOfDataflowExecutions.get(0).get(DataflowExecutions.MetadataKey.state));
        data.setBeginDate(switchLongToDate(listOfDataflowExecutions.get(0).get(DataflowExecutions.MetadataKey.beginTime)));
        data.setEndDate(setEndTimeOfExecutionDataflow(listOfDataflowExecutions.get(0)));
        data.setExpectedEndDate(expectedTime);
        data.setOnTime(compareEndAndExpectedTime(listOfDataflowExecutions.get(0), expectedTime));
        data.setLastDuration(getTimeOfLastDuration(listOfDataflowExecutions.get(0)));
        data.setAvgDuration(getAvgDuration(listOfDataflowExecutions));
        LinkedHashMap<String, Integer> listOfOnboardFlowVersion = dataflow.get(Dataflow.MetadataKey.onboardFlowVersion);
        data.setVersion(listOfOnboardFlowVersion.get("versionNumber"));

        data.setRowsRead(getDataFromDataflowAsLong(listOfDataflowExecutions.get(0), DataflowExecutions.MetadataKey.totalRowsRead));
        data.setAvgRowsRead(getAvgRowsRead(listOfDataflowExecutions));

        data.setBytesRead(getDataFromDataflowAsLong(listOfDataflowExecutions.get(0), DataflowExecutions.MetadataKey.totalBytesRead));
        data.setAvgBytesRead(getAvgBytesRead(listOfDataflowExecutions));

        data.setRowsWrite(setTotalRowsWrittenOfExecutionDataflow(listOfDataflowExecutions.get(0)));
        data.setRowsWriteLastTime(setTotalRowsWrittenOfExecutionDataflow(listOfDataflowExecutions.get(1)));
        data.setAvgRowsWrite(getAvgRowsWrite(listOfDataflowExecutions));
        data.setBytesWrite(setTotalBytesWrittenOfExecutionDataflow(listOfDataflowExecutions.get(0)));
        data.setAvgBytesWrite(getAvgBytesWrite(listOfDataflowExecutions));
        return data;
    }

    private long getDataFromDataflowAsLong(DataflowExecutions dataflowExecution, DataflowExecutions.MetadataKey metadataKey) {
        Number dataDataflowExecutionNumber = dataflowExecution.get(metadataKey);
        try {
            TimeUnit.MILLISECONDS.sleep(20);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return dataDataflowExecutionNumber.longValue();
    }

    private String getAvgDuration(List<DataflowExecutions> listOfDataflowExecutions) {
        long sumOfDurations = listOfDataflowExecutions.stream().mapToLong(this::getTimeOfDuration).sum();
        long timeAvgInMiliseconds = sumOfDurations / listOfDataflowExecutions.size();
        return timeToString(timeAvgInMiliseconds);
    }

    private String timeToString(long time){
        int minutes = (int) Math.floor((time) / 60000);
        int seconds = (int) Math.floor((time - minutes * 60000) / 1000);
        if (minutes == 0 && seconds < 10) {
            return minutes + "m 0" + seconds + "s";
        } else if (minutes >= 60) {
            int hours = (int) Math.floor((minutes) / 60);
            minutes = minutes % 60;
            if (minutes > 0){
                return hours + "h " + minutes + "m " + seconds + "s";
            } else {
                return hours + "h " + seconds + "s";
            }
        } else {
            return minutes + "m " + seconds + "s";
        }
    }

    private long getTimeOfDuration(DataflowExecutions dataflowExecution) {
        long beginTime = dataflowExecution.get(DataflowExecutions.MetadataKey.beginTime);
        long endTime;
        try {
            endTime = dataflowExecution.get(DataflowExecutions.MetadataKey.endTime);
        } catch (NullPointerException e) {
            endTime = new Date().getTime();
        }
        long difference = (endTime - beginTime);
        return difference;
    }

    private String getTimeOfLastDuration(DataflowExecutions dataflowExecutions) {
        long difference = getTimeOfDuration(dataflowExecutions);
        return timeToString(difference);
    }

    private Long getAvgBytesWrite(List<DataflowExecutions> listOfDataflowExecutions) {
        List<DataflowExecutions> listOfSuccessDataflowExecutions = getListOfSuccessDataflowExecutions(listOfDataflowExecutions);
        long sumOfBytesWrite = listOfSuccessDataflowExecutions.stream().mapToLong(this::getAvgBytesWriteFromDataflowExecution).sum();
        long avgBytesWrite = sumOfBytesWrite / listOfSuccessDataflowExecutions.size();
        return avgBytesWrite;
    }

    private Long getAvgBytesWriteFromDataflowExecution(DataflowExecutions dataflowExecutions) {
        return getDataFromDataflowAsLong(dataflowExecutions, DataflowExecutions.MetadataKey.totalBytesWritten);
    }

    private Long getAvgRowsWrite(List<DataflowExecutions> listOfDataflowExecutions) {
        List<DataflowExecutions> listOfSuccessDataflowExecutions = getListOfSuccessDataflowExecutions(listOfDataflowExecutions);
        long sumOfRowsWrite = listOfSuccessDataflowExecutions.stream().mapToLong(this::getAvgRowsWriteFromDataflowExecution).sum();
        long avgRowsWrite = sumOfRowsWrite / listOfSuccessDataflowExecutions.size();
        return avgRowsWrite;
    }

    private Long getAvgRowsWriteFromDataflowExecution(DataflowExecutions dataflowExecutions) {
        return getDataFromDataflowAsLong(dataflowExecutions, DataflowExecutions.MetadataKey.totalRowsWritten);
    }

    private Long getAvgBytesRead(List<DataflowExecutions> listOfDataflowExecutions) {
        List<DataflowExecutions> listOfSuccessDataflowExecutions = getListOfSuccessDataflowExecutions(listOfDataflowExecutions);
        long sumOfBytesRead = listOfSuccessDataflowExecutions.stream().mapToLong(this::getAvgBytesReadFromDataflowExecution).sum();
        long avgBytesRead = sumOfBytesRead / listOfSuccessDataflowExecutions.size();
        return avgBytesRead;
    }

    private Long getAvgBytesReadFromDataflowExecution(DataflowExecutions dataflowExecutions) {
        return getDataFromDataflowAsLong(dataflowExecutions, DataflowExecutions.MetadataKey.totalBytesRead);
    }

    private Long getAvgRowsRead(List<DataflowExecutions> listOfDataflowExecutions) {
        List<DataflowExecutions> listOfSuccessDataflowExecutions = getListOfSuccessDataflowExecutions(listOfDataflowExecutions);
        long sumOfRowsRead = listOfSuccessDataflowExecutions.stream().mapToLong(this::getAvgRowsReadFromDataflowExecution).sum();
        long avgRowsRead = sumOfRowsRead / listOfSuccessDataflowExecutions.size();
        return avgRowsRead;
    }

    private Long getAvgRowsReadFromDataflowExecution(DataflowExecutions dataflowExecution) {
        return getDataFromDataflowAsLong(dataflowExecution, DataflowExecutions.MetadataKey.totalRowsRead);
    }

    private Long setTotalRowsWrittenOfExecutionDataflow(DataflowExecutions dataflowExecution) {
        long totalRowsWrittenOfProcessing = 0;
        try {
            totalRowsWrittenOfProcessing = getDataFromDataflowAsLong(dataflowExecution, DataflowExecutions.MetadataKey.totalRowsWritten);
        } catch (NullPointerException e) {
            return totalRowsWrittenOfProcessing;
        }
        return totalRowsWrittenOfProcessing;
    }

    private Long setTotalBytesWrittenOfExecutionDataflow(DataflowExecutions dataflowExecution) {
        long totalBytesWrittenOfProcessing = 0;
        try {
            totalBytesWrittenOfProcessing = getDataFromDataflowAsLong(dataflowExecution, DataflowExecutions.MetadataKey.totalBytesWritten);
        } catch (NullPointerException e) {
            return totalBytesWrittenOfProcessing;
        }
        return totalBytesWrittenOfProcessing;
    }

    private List<DataflowExecutions> getListOfSuccessDataflowExecutions(List<DataflowExecutions> listOfDataflowExecutions) {
        List<DataflowExecutions> listOfNotFailedDataflowExecutions = new LinkedList<>();
        for (DataflowExecutions dataflowExecutions : listOfDataflowExecutions) {
            if (dataflowExecutions.get(DataflowExecutions.MetadataKey.state).equals("SUCCESS")) {
                listOfNotFailedDataflowExecutions.add(dataflowExecutions);
            }
        }
        return listOfNotFailedDataflowExecutions;
    }

    private String getExpectedDate(List<String> listOfData) {
        String expectedDate;
        String expectedTime;
        if (listOfData.size() == 2) {
            Date curDate = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            expectedDate = sdf.format(curDate);
            expectedTime = listOfData.get(1);
        } else {
            expectedDate = listOfData.get(1);
            expectedTime = listOfData.get(2);
        }
        String expectedDateAndTime = new StringBuilder().append(expectedDate).append(" ").append(expectedTime).toString();
        return expectedDateAndTime;
    }
}
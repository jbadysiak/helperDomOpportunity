package com.domo.support.app.domo;

public class Data {
    private String name;
    private String id;
    private String status;
    private String type;
    private String beginDate;
    private String endDate;
    private String expectedEndDate;
    private String onTime;
    private String lastDuration;
    private String avgDuration;
    private Integer version;
    private Long rowsRead;
    private Long avgRowsRead;
    private Long bytesRead;
    private Long avgBytesRead;
    private Long rowsWrite;
    private Long rowsWriteLastTime;
    private Long avgRowsWrite;
    private Long bytesWrite;
    private Long avgBytesWrite;


    public Data() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getExpectedEndDate() {
        return expectedEndDate;
    }

    public void setExpectedEndDate(String expectedEndDate) {
        this.expectedEndDate = expectedEndDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getLastDuration() {
        return lastDuration;
    }

    public void setLastDuration(String lastDuration) {
        this.lastDuration = lastDuration;
    }

    public String getAvgDuration() {
        return avgDuration;
    }

    public void setAvgDuration(String avgDuration) {
        this.avgDuration = avgDuration;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Long getRowsRead() {
        return rowsRead;
    }

    public void setRowsRead(Long rowsRead) {
        this.rowsRead = rowsRead;
    }

    public Long getAvgRowsRead() {
        return avgRowsRead;
    }

    public void setAvgRowsRead(Long avgRowsRead) {
        this.avgRowsRead = avgRowsRead;
    }

    public Long getBytesRead() {
        return bytesRead;
    }

    public void setBytesRead(Long bytesRead) {
        this.bytesRead = bytesRead;
    }

    public Long getAvgBytesRead() {
        return avgBytesRead;
    }

    public void setAvgBytesRead(Long avgBytesRead) {
        this.avgBytesRead = avgBytesRead;
    }

    public Long getRowsWrite() {
        return rowsWrite;
    }

    public void setRowsWrite(Long rowsWrite) {
        this.rowsWrite = rowsWrite;
    }

    public Long getRowsWriteLastTime() {
        return rowsWriteLastTime;
    }

    public void setRowsWriteLastTime(Long rowsWriteLastTime) {
        this.rowsWriteLastTime = rowsWriteLastTime;
    }

    public Long getAvgRowsWrite() {
        return avgRowsWrite;
    }

    public void setAvgRowsWrite(Long avgRowsWrite) {
        this.avgRowsWrite = avgRowsWrite;
    }

    public Long getBytesWrite() {
        return bytesWrite;
    }

    public void setBytesWrite(Long bytesWrite) {
        this.bytesWrite = bytesWrite;
    }

    public Long getAvgBytesWrite() {
        return avgBytesWrite;
    }

    public void setAvgBytesWrite(Long avgBytesWrite) {
        this.avgBytesWrite = avgBytesWrite;
    }

    public String getOnTime() {
        return onTime;
    }

    public void setOnTime(String onTime) {
        this.onTime = onTime;
    }
}
